<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Raquel Pont Lillo <raquel.pont.lillo@gmail.com>
 */
interface Model {
    public function createCurso($curso);
    public function readCursos();
    public function createAlumno($alumno);
    public function readAlumnos();
    public function instalar();
    public function desinstalar();
}
