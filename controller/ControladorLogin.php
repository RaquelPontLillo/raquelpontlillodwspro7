<?php

if (!isset($_SESSION)) {
    session_start();
}

include_once __DIR__ . '/Funciones.php';

$usuario = recoge("usuario");
$password = recoge("password");
if ($usuario === "root" && $password === "pass") {
    $_SESSION["autenticar"] = "autenticado";
    header("Location: ../view/VistaModelo.php");
} else {
    header("Location: ../index.php");
}