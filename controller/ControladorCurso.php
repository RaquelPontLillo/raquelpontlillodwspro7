<?php

include_once __DIR__.'/../model/Files.php';
include_once __DIR__.'/../model/Mysql.php';
include_once __DIR__.'/../model/Curso.php';
include_once __DIR__.'/Funciones.php';

/**
 *
 * @author Raquel Pont Lillo <raquel.pont.lillo@gmail.com>
 */

function listar() {
    $cursos = modelo();
    foreach ($cursos->readCursos() as $curso) {
        echo "<tr>
                <td>" . $curso->__GET('id') . "</td>
                <td>" . $curso->__GET('nombre') . "</td>
    		<td>" . $curso->__GET('horas') . "</td>
            </tr>";
    }
}

function calcularID() {
    $modelo = modelo();
    echo $modelo->idCurso();
}

function guardar() {
    $id = recoge('id');
    $nombre = recoge('nombre');
    $horas = recoge('horas');

    if ($id == "" || $nombre == "" || $horas == "") {
        echo "<p style='color:red'>Alguno de los campos está vacío. Rellénelos primero.</p>"
        . "<p><a href='../index.php'>Volver a inicio</a> || <a href='../view/VistaCurso.php'>Volver a gestión de cursos</a></p>";
    } else {
        $curso = new Curso($id, $nombre, $horas);
        $modelo = modelo();
        $modelo->createCurso($curso);
        echo "<p style='color:blue'>Los datos se han guardado con éxito.</p>"
        . "<p><a href='../index.php'>Volver a inicio</a> || <a href='../view/VistaCurso.php'>Volver a gestión de cursos</a></p>";
    }
}
