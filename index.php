<!DOCTYPE html>

<html>
    <head>
        <title>Vista login | Matrículas App. 2016-2017</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="shortcut icon" href="media/images/kandel.ico">
        <link rel="stylesheet" href="media/css/pure-min.css">
    </head>
    <body>
        <?php include_once __DIR__.'/view/Partials.php';
        myheader();
        ?>
        <h2>Acceso restringido: identifíquese</h2>
        <div class="pure-g">
            <div class="pure-u-1-12">
                <form action="controller/ControladorLogin.php" method="post" class="pure-form pure-form-stacked">
                    <table>
                        <tr>
                            <th>Usuario:</th>
                            <td><input type="text" name="usuario" value="root"  /></td>
                        </tr>
                        <tr>
                            <th>Contraseña:</th>
                            <td><input type="password" name="password" value="pass"  /></td>
                        </tr>
                        <tr>
                            <th><input type="submit" value="Acceder" class="pure-button pure-button-primary" /></th>
                            <td><input type="reset" value="Borrar" class="pure-button pure-button-primary" /></td>
                        </tr>
                    </table>
                </form>
            </div>
        </div>
        <br/>
        <?php myfooter(); ?>
    </body>
</html>