<?php
include_once __DIR__.'/../controller/Funciones.php';
include_once __DIR__.'/Partials.php';
acceder();
?>
<!DOCTYPE html>
<html>
    <head>
        <title>Vista modelo | Matrículas App. 2016-2017</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="shortcut icon" href="../media/images/kandel.ico">
        <link rel="stylesheet" href="../media/css/pure-min.css">
        <link rel="stylesheet" href="../media/css/styles.css">
    </head>
    <body>
        <?php myheader(); 
        salir(); ?>
        <h2>¿Qué sistema quieres usar para guardar los datos?</h2>
        <div class="pure-g">
            <div class="pure-u-1-12">
                <form action="../controller/ControladorInicio.php" method="post" class="pure-form pure-form-stacked" >
                    <select name="modelo">
                        <option value="ficheros">Ficheros CSV</option>
                        <option value="mysql">Base de datos MySQL</option>
                    </select>
                    <p>¿Instalar base de datos? <input type="checkbox" name="database" value="active" /></p><br />
                    <button type="submit" class="pure-button pure-button-primary">Continuar</button>
                </form>
            </div>
        </div>
        <br/>
        <?php myfooter(); ?>
    </body>
</html>