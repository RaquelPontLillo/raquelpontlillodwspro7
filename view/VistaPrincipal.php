<?php
include_once __DIR__.'/../controller/Funciones.php';
include_once __DIR__.'/Partials.php';
acceder();
?>
<!DOCTYPE html>
<html>
    <head>
        <title>Vista principal | Matrículas App. 2016-2017</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="../media/css/pure-min.css">
        <link rel="stylesheet" href="../media/css/styles.css">
        <link rel="shortcut icon" href="../media/images/kandel.ico">
    </head>
    <body>
        <?php
        mymenu();
        myheader();
        salir();
        ?>
            <h2>¿Qué quieres hacer?</h2>
        <div class="pure-g">
            <div class="pure-u-1-12">
                <table>
                    <tr>
                        <th>Gestionar cursos</th>
                        <td><a href="VistaCurso.php"><img src ="../media/images/curso.jpg"/></a></td>
                    </tr>
                    <tr>
                        <th>Gestionar alumnos</th>
                        <td><a href="VistaAlumno.php"><img src ="../media/images/alumno.jpg"/></a></td>
                    </tr>
                    <tr>
                        <th>Ver documentación</th>
                        <td><a href="../media/docs/documentacion.pdf"><img src ="../media/images/documentacion.jpg"/></a></td>
                    </tr>
                </table>
            </div>
        </div>
        <?php myfooter(); ?>
    </body>
</html>
