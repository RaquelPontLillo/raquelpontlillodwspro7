<?php
include_once __DIR__.'/Config.php';

function myheader() {
    echo "<h1>" . Config::$titulo . "</h1><hr/>\n";
}

function myfooter() {
    echo "<hr/><pre>" . Config::$empresa . " " . Config::$autor . " ";
    echo Config::$anio . " " . Config::$fecha . "</pre>\n";
}

function salir() {
    echo '<form action="../controller/ControladorLogout.php" method="post">
          <input type="submit" value="Cerrar sesión" class="pure-button pure-button-primary" /></form>';
}

function mymenu() {
    echo "<ul>
            <li><a href='VistaPrincipal.php'>Menú inicial</a></li>
            <li><a href='VistaCurso.php'>Gestión de cursos</a></li>
            <li><a href='VistaAlumno.php'>Gestión de alumnos</a></li>
            <li><a href='../media/docs/documentacion.pdf'>Documentación</a></li>
        </ul>";
}   