<?php
include_once __DIR__.'/Partials.php';
include_once __DIR__.'/../controller/Funciones.php';
include_once __DIR__.'/../controller/ControladorAlumno.php';
acceder();
?>
<!DOCTYPE html>
<html lang="es">
    <head>
        <title>Vista de alumnos | Matrículas App. 2016-2017</title>
        <link rel="stylesheet" href="../media/css/pure-min.css">
        <link rel="stylesheet" href="../media/css/styles.css">
        <link rel="shortcut icon" href="../media/images/kandel.ico">
        <meta charset="UTF-8">
    </head>
    <body>
        <?php
        mymenu();
        myheader();
        salir();
        ?>
        <h2>Gestión de alumnos</h2>
        <div class="pure-g">
            <div class="pure-u-1-12">
                <form onsubmit="<?php guardar(); ?>" method="post" class="pure-form pure-form-stacked" >
                    <table>
                        <tr>
                            <th>ID:</th>
                            <td><input type="text" name="id" value="<?php calcularID() ?>" readonly="readonly" /></td>
                        </tr>
                        <tr>
                            <th>Nombre:</th>
                            <td><input type="text" name="nombre" value=""  /></td>
                        </tr>
                        <tr>
                            <th>Curso:</th>
                            <td>
                                <select name="curso" >
                                    <option value="0">-- Escoge un curso --</option>
                                    <?php cargaCombo(); ?>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <button type="submit" class="pure-button pure-button-primary">Dar de alta</button>
                            </td>
                        </tr>
                    </table>
                </form>
                
                <table class="pure-table pure-table-horizontal">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Nombre</th>
                            <th>ID curso</th>
                            <!--<th colspan="2">Acciones</th>-->
                        </tr>
                    </thead>
                    <?php listar(); ?>
                </table>     
            </div>
        </div>
        <?php myfooter(); ?>
    </body>
</html>
